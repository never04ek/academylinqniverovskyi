using System;

namespace AcademyLINQNiverovskyi.entities
{
    public class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return 
                $"Team = {{id : {id}, \nName : {name}, \nCreatedAt : {CreatedAt}}}";

        }
    }
}