using System;

namespace AcademyLINQNiverovskyi.entities
{
    public class Tasks
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime finished_at { get; set; }
        public TaskState state { get; set; }
        public int project_id { get; set; }
        public int performer_id { get; set; }
        
        public override string ToString()
        {
            return
                $"Task = {{id : {id}, \nName : {name}, \nDescription : {description},\nCreatedAt : {created_at},\nFinishedAt : {finished_at},\nState : {state},\nProjectId : {project_id},\nPerformerId : {performer_id}}}";
        }
    }
}