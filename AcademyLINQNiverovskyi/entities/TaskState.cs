namespace AcademyLINQNiverovskyi.entities
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}