using System;

namespace AcademyLINQNiverovskyi.entities
{
    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime deadline { get; set; }
        public int author_id { get; set; }
        public int team_id { get; set; }

        public override string ToString()
        {
            return
                $"Project = {{id : {id}, \nName : {name}, \nDescription : {description},\nCreatedAt : {created_at},\nDeadline : {deadline},\nAuthorId : {author_id},\nTeamId : {team_id}}}";
        }
    }
}