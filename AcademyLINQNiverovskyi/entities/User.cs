using System;

namespace AcademyLINQNiverovskyi.entities
{
    public class User
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public DateTime birthday { get; set; }
        public DateTime registered_at { get; set; }
        public int? team_id { get; set; }

        public override string ToString()
        {
            return
                $"User = {{ id : {id}, \nFirstName : {first_name}, LastName : {last_name},\nEmail : {email},\nBirthday : {birthday},\nRegisteredAt : {registered_at},\nTeamId : {team_id}}}";

        }
    }
}