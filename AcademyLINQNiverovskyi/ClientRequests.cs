using System.Collections.Generic;
using AcademyLINQNiverovskyi.entities;
using System.Net;
using Newtonsoft.Json;

namespace AcademyLINQNiverovskyi
{
    public static class ClientRequests
    {
        
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://bsa2019.azurewebsites.net/api";
        
        public static Project GetProject(int id)
        {
            return JsonConvert.DeserializeObject<Project>(Client.DownloadString($"{_path}/Projects/{id}"));
        }

        public static List<Project> GetProjects()
        {
            return JsonConvert.DeserializeObject<List<Project>>(Client.DownloadString($"{_path}/Projects"));

        }

        public static List<Tasks> GetTasks()
        {
            return JsonConvert.DeserializeObject<List<Tasks>>(Client.DownloadString($"{_path}/Tasks"));

        }

        public static Tasks GetTask(int id)
        {
            return JsonConvert.DeserializeObject<Tasks>(Client.DownloadString($"{_path}/Tasks/{id}"));

        }
        
//        public static List<TaskState> GetTaskStatesList()
//        {
//            return JsonConvert.DeserializeObject<List<TaskState>>(Client.DownloadString($"{_path}/TaskStates"));
//
//        }
        
        public static Team GetTeam(int id)
        {
            return JsonConvert.DeserializeObject<Team>(Client.DownloadString($"{_path}/Teams{id}"));

        }

        public static List<Team> GetTeams()
        {
            return JsonConvert.DeserializeObject<List<Team>>(Client.DownloadString($"{_path}/Teams"));
        }
        
        public static User GetUser(int id)
        {
            return JsonConvert.DeserializeObject<User>(Client.DownloadString($"{_path}/Users/{id}"));

        }

        public static List<User> GetUsers()
        {
            return JsonConvert.DeserializeObject<List<User>>(Client.DownloadString($"{_path}/Users"));

        }
    }
}