﻿using System;
using System.Collections.Generic;
using System.Linq;
using AcademyLINQNiverovskyi.entities;

namespace AcademyLINQNiverovskyi
{
    class Program
    {
        #region Task1

        private static Dictionary<Project, int> GetAmountOfUserTasksInEachProject(int id)
        {
            var projects = ClientRequests.GetProjects();
            return ClientRequests.GetTasks()
                .Where(task => task.performer_id == id)
                .GroupBy(task => task.project_id)
                .ToDictionary(group => projects[group.Key - 1], group => group.Count());
        }

        #endregion

        #region Task2

        private static List<Tasks> GetUserTasks45Name(int id)
        {
            return ClientRequests.GetTasks()
                .Where(task => task.performer_id == id)
                .Where(task => task.name.Length < 45)
                .ToList();
        }

        #endregion

        #region Task3

        private static List<(int, string)> GetThisYearTasksOfUser(int id)
        {
            return ClientRequests.GetTasks()
                .Where(task => task.performer_id == id)
                .Where(task => task.finished_at.Year == 2019)
                .Select(task => (task.id, task.name))
                .ToList();
        }

        #endregion

        #region Task4

        private static List<(int?, string, List<User>)> GetTeamsAndUsers()
        {
            //Displays nothing, because all teams have participants under 12 years old.
            var users = ClientRequests.GetUsers();
            var teams = ClientRequests.GetTeams();
            return users
                .Where(user => user.team_id != null)
                .GroupBy(user => user.team_id)
                .Where(team =>
                    team.All(user => DateTime.Now.Year - user.birthday.Year > 12))
                .Select(team => (team.Key, teams[(int) team.Key - 1].name,
                    team.OrderByDescending(user => user.registered_at).ToList()))
                .ToList();
        }

        #endregion

        #region Task5

        private static List<(User, List<Tasks>)> GetUserAndTasks()
        {
            var users = ClientRequests.GetUsers();
            return ClientRequests.GetTasks()
                .GroupBy(task => task.performer_id)
                .Select(userT => (users[userT.Key - 1], userT.OrderByDescending(task => task.name.Length).ToList()))
                .OrderBy(user => user.Item1.first_name)
                .ToList();
        }

        #endregion

        #region Task6

        private static (User, Project, int, int, Tasks)? GetSpecUserStruct(int id)
        {
            var user = ClientRequests.GetUser(id);
            var projects = ClientRequests.GetProjects();
            var tasks = ClientRequests.GetTasks();
            var res = projects
                .Where(project => project.author_id == user.id)
                .Where(project =>
                    project.created_at == projects
                        .Where(projectmin => projectmin.author_id == user.id)
                        .Max(projectmin => projectmin.created_at))
                .Select(project => (user,
                        project,
                        tasks.Count(task => project.id == task.project_id),
                        tasks.Where(task => task.performer_id == user.id)
                            .Count(task => task.state == TaskState.Finished || task.state == TaskState.Canceled),
                        tasks.Where(task => task.performer_id == user.id)
                            .OrderBy(task => task.finished_at - task.created_at).Last()
                    )).FirstOrDefault();


            return res;
        }

        #endregion

        #region Task7

        private static (Project, Tasks, Tasks, int?)? GetSpecProjectStruct(int id)
        {
            var users = ClientRequests.GetUsers();
            var projects = ClientRequests.GetProjects();
            var tasks = ClientRequests.GetTasks();
            var res = tasks.Where(task => task.project_id == id)
                .Where(task =>
                    task.description.Length == tasks.Where(taskd => taskd.project_id == id)
                        .Max(taskd => taskd.description.Length)).Select(task => (projects[id - 1], task,
                        tasks
                            .Where(taskn =>
                                taskn.project_id == id).FirstOrDefault(taskn =>
                                taskn.name.Length == tasks.Where(tasknn => tasknn.project_id == id)
                                    .Min(tasknn => tasknn.name.Length)),
                        users.Count(user => user.team_id == projects[id - 1].team_id &&
                                            (projects[id - 1].description.Length > 25 ||
                                             tasks.Count(taskc => taskc.project_id == id) < 3)) == 0
                            ? null
                            : (int?) users.Count(user =>
                                user.team_id == projects[id - 1].team_id)
                    )).FirstOrDefault();

            return res;
        }

        #endregion

        private static void Main()
        {
            Console.WriteLine("Task 1");
            foreach (var i in GetAmountOfUserTasksInEachProject(1))
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("========================");
            Console.WriteLine("Task 2");
            foreach (var i in GetUserTasks45Name(2))
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("========================");
            Console.WriteLine("Task 3");
            foreach (var i in GetThisYearTasksOfUser(36))
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("========================");
            Console.WriteLine("Task 4");
            Console.WriteLine("(Displays nothing, because all teams have participants under 12 years old.)");
            foreach (var i in GetTeamsAndUsers())
            {
                Console.WriteLine($"id = {i.Item1}, name = {i.Item2}");
                foreach (var user in i.Item3)
                {
                    Console.WriteLine(user);
                }
            }
            Console.WriteLine("========================");
            Console.WriteLine("Task 5");
            foreach (var i in GetUserAndTasks())
            {
                Console.WriteLine(i.Item1);
                foreach (var task in i.Item2)
                {
                    Console.WriteLine(task);
                }
            }
            Console.WriteLine("========================");
            Console.WriteLine("Task 6");
            Console.WriteLine(GetSpecUserStruct(43));
            Console.WriteLine("========================");
            Console.WriteLine("Task 7");
            Console.WriteLine(GetSpecProjectStruct(36));
        }
    }
}